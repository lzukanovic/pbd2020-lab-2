package si.uni_lj.fri.pbd.lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // Get the name from the intent
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String name = "";
        if(bundle != null)
            name = bundle.getString("EXTRA_NAME");

        // Change the text to user's name
        TextView textView = (TextView) findViewById(R.id.text_name);
        textView.setText(name);

        // Listen for button press
        Button button = (Button) findViewById(R.id.button_message_toast);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getApplicationContext();
                EditText editMsg = (EditText) findViewById(R.id.edit_message_of_the_day);
                String msg = editMsg.getText().toString();
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, msg, duration);
                toast.show();
            }
        });
    }
}
