package si.uni_lj.fri.pbd.lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    }

    public void registerUser(View view) {
        EditText editText = (EditText) findViewById(R.id.edit_name);
        String input = editText.getText().toString();

        if(!(input.trim().length() > 0)) {
            editText.setError(getText(R.string.reg_full_name_error));
        } else {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra("EXTRA_NAME", input);
            startActivity(intent);
        }
    }
}
